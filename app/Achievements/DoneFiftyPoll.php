<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class DoneFiftyPoll extends Achievement
{
    public $id = "poll-ten";
    /*
     * The achievement name
     */
    public $name = 'Протестируй себя 50 раз';

    /*
     * A small description for the achievement
     */
    public $description = 'Поздравляю, вы прошли 50 тестов';

    public $points = 50;
}
