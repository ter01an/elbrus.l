<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class DoneFirstPoll extends Achievement
{
    public $id = "poll-first";
    /*
     * The achievement name
     */
    public $name = 'Протестируй себя';

    /*
     * A small description for the achievement
     */
    public $description = 'Поздравляю, вы прошли первый тест';
}
