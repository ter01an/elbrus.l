<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\AchievementChain;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class DonePolls extends AchievementChain
{
    /*
     * Returns a list of instances of Achievements
     */
    public function chain(): array
    {
        return [new DoneFirstPoll(), new DoneTenPoll(), new DoneTwentyPoll(), new DoneFiftyPoll()];
    }
}
