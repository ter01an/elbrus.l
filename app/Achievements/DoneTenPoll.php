<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class DoneTenPoll extends Achievement
{
    public $id = "poll-ten";
    /*
     * The achievement name
     */
    public $name = 'Протестируй себя 10 раз';

    /*
     * A small description for the achievement
     */
    public $description = 'Поздравляю, вы прошли 10 тестов';

    public $points = 10;
}
