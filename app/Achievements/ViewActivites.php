<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\AchievementChain;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class ViewActivites extends AchievementChain
{
    /*
     * Returns a list of instances of Achievements
     */
    public function chain(): array
    {
        return [new ViewFirstActivites(), new ViewTenActivites(), new ViewTwentyActivites(), new ViewFiftyActivites()];
    }
}
