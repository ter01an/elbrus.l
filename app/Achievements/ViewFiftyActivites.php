<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class ViewFiftyActivites extends Achievement
{
    public $id = "activities-fifty";
    /*
     * The achievement name
     */
    public $name = 'Принять участие в 50 мероприятиях';

    /*
     * A small description for the achievement
     */
    public $description = 'Поздравляю, вы приняли участие в 50 мероприятиях';

    public $points = 50;
}
