<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class ViewFirstActivites extends Achievement
{
    public $id = "activities-first";
    /*
     * The achievement name
     */
    public $name = 'Принять участие в мероприятии';

    /*
     * A small description for the achievement
     */
    public $description = 'Поздравляю, вы приняли участие в своем первом мероприятии';
}
