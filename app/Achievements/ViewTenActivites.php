<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class ViewTenActivites extends Achievement
{
    public $id = "activities-ten";
    /*
     * The achievement name
     */
    public $name = 'Принять участие в 10 мероприятиях';

    /*
     * A small description for the achievement
     */
    public $description = 'Поздравляю, вы приняли участие в 10 мероприятиях';

    public $points = 10;
}
