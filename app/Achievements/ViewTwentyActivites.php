<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class ViewTwentyActivites extends Achievement
{
    public $id = "activities-twenty";
    /*
     * The achievement name
     */
    public $name = 'Принять участие в 20 мероприятиях';

    /*
     * A small description for the achievement
     */
    public $description = 'Поздравляю, вы приняли участие в 20 мероприятиях';

    public $points = 20;
}
