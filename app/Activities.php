<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activities extends Model
{
    protected $fillable = ['title', 'image', 'text', 'embed', 'online'];

    public function users() {
        return $this->belongsToMany(User::class, 'activities_registrations')
            ->whereNull('activities_registrations.deleted_at')
            ->withTimestamps();
    }
}
