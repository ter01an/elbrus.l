<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivitiesReferral extends Model
{
    protected $fillable = ['user_id', 'referred_id', 'activities_id'];
}
