<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivitiesRegistration extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'activities_id'];
}
