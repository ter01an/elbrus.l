<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivitiesTime extends Model
{
    protected $fillable = ['user_id', 'activities_id', 'time', 'total'];
}
