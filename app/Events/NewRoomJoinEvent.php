<?php

namespace App\Events;

use App\Message;
use App\Room;
use App\User;
use App\Services\MessageService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewRoomJoinEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $room_id;
    protected $user;

    /**
     * Create a new event instance.
     *
     * @param Room $room
     * @param User $user
     */
    public function __construct(Room $room, User $user)
    {
        $this->room_id = $room->id;
        $this->user = $user->only(['id', 'name', 'avatar']);
    }

    public function broadcastWith()
    {
        return $this->user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel("room.join.{$this->room_id}");
    }

    public function broadcastAs()
    {
        return 'Join.New';
    }
}
