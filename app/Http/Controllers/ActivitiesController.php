<?php

namespace App\Http\Controllers;

use App\Activities;
use App\ActivitiesRegistration;
use App\Services\ActivitiesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ActivitiesController extends Controller
{
    public function item(Request $request, $id)
    {
        $item = Activities::findOrFail($id);

        return view('pages.activite', ['item' => $item]);
    }

    public function itemReg(Request $request, $id)
    {
        $item = Activities::find($id);
        $item->users()->attach(Auth()->user()->id);

        return redirect()->back();
    }

    public function itemUnreg(Request $request, $id)
    {
        ActivitiesRegistration::where('user_id', Auth()->user()->id)->where('activities_id', $id)->update(['deleted_at' => DB::raw('NOW()')]);

        return redirect()->back();
    }

    public function qr($id)
    {
        $item = Activities::findOrFail($id);
        $link = Auth()->check() ? ActivitiesService::getReferralLink($item, Auth()->user()) : route('activities.item', ['id' => $id]);

        return QrCode::size(500)->generate($link);
    }
}
