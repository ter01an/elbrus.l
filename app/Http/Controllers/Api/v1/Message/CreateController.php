<?php

namespace App\Http\Controllers\Api\v1\Message;

use App\Http\Controllers\Controller;
use App\Http\Requests\MessageCreateRequest;
use App\Services\MessageService;
use Illuminate\Support\Facades\Auth;

class CreateController extends Controller
{
    public function __invoke(MessageCreateRequest $request)
    {
        $message = MessageService::create(
            Auth::user(),
            $request->get('to_id'),
            $request->get('to_type'),
            $request->get('text')
        );

        return MessageService::info($message);
    }
}
