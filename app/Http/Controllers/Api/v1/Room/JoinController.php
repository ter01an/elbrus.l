<?php

namespace App\Http\Controllers\Api\v1\Room;

use App\Http\Controllers\Controller;
use App\Room;
use App\User;
use App\Services\RoomService;
use Illuminate\Support\Facades\Auth;

class JoinController extends Controller
{
    public function __invoke(Room $room, User $user)
    {
        return RoomService::join($room, $user);
    }
}
