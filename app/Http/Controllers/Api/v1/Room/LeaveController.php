<?php

namespace App\Http\Controllers\Api\v1\Room;

use App\Http\Controllers\Controller;
use App\Room;
use App\Services\RoomService;
use Illuminate\Support\Facades\Auth;

class LeaveController extends Controller
{
    public function __invoke(Room $room)
    {
        return [
            'success' => RoomService::leave($room, Auth::user())
        ];
    }
}
