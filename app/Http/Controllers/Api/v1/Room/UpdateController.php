<?php

namespace App\Http\Controllers\Api\v1\Room;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoomUpdateRequest;
use App\Room;
use App\Services\RoomService;

class UpdateController extends Controller
{
    public function __invoke(RoomUpdateRequest $request, Room $room)
    {
        RoomService::update($room, $request->get('title'));

        return [
            'room' => $room->only(['id', 'title', 'readonly'])
        ];
    }
}
