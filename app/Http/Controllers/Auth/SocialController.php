<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function login()
    {
        return view('pages.login');
    }

    public function register(Request $request)
    {
        if(!$request->session()->has('user_id'))
        {
            return redirect()->route('home');
        }

        if($request->method() === 'POST')
        {
            $user_id = $request->session()->pull('user_id');
            $request->session()->forget('user_id');

            $user = User::find($user_id);

            $user->phone = $request->get('phone');
            $user->active = 1;
            $user->save();

            auth()->login($user, true);

            if($act_ref = request()->cookie('activities_referral_id', 0)) {
                return redirect()->route('activities.item', ['id' => $act_ref]);
            }

            return redirect()->route('home');
        }

        return view('pages.register');
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('home');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $socialiteUser = Socialite::driver($provider)->user();

        $user = UserService::findOrCreateUser($provider, $socialiteUser);

        if($user->active == 1) {
            auth()->login($user, true);
        } else {
            request()->session()->put('user_id', $user->id);
        }

        return $user->active ? redirect()->route('cabinet') : redirect()->route('register');
    }
}
