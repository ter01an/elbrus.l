<?php

namespace App\Http\Controllers;

use App\Achievements\DonePolls;
use App\Activities;
use App\PollAnswers;
use App\PollQuestions;
use App\PollResult;
use App\Polls;
use App\Room;
use App\Services\AchievementService;
use App\Services\UserService;
use App\Services\VkService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CabinetController extends Controller
{
    public function __invoke($id = false)
    {
        $achievements = AchievementService::get();
        $user = $id ? User::findOrFail($id) : Auth()->user();

        return view('cabinet.index', [
            'achievements' => $achievements,
            'user' => $user
        ]);
    }

    public function analytic()
    {
        $users = DB::table('users')
            ->select(DB::raw('COUNT(id) AS count, YEAR(created_at) AS created_year, MONTH(created_at) AS created_month'))
            ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
            ->orderBy('created_month')->get();

        $users = $users->keyBy('created_month');

        $users_all = User::count();
        $broadcast_times = DB::table('activities_times')
            ->select(DB::raw('activities_id, COUNT(*) as users, MAX(total) as total, SUM(time) / COUNT(*) as time'))
            ->groupBy('activities_id')->limit(10)->get();

        $broadcast_stat = 0;

        $total_time = 0;
        $total_all = 0;

        $total_time_off = 0;
        $total_all_off = 0;

        $total_count = 0;
        foreach ($broadcast_times as &$broadcast_time) {

            $item = Activities::find($broadcast_time->activities_id);

            if($item->online) {
                $total_time += $broadcast_time->time;
                $total_all += $broadcast_time->total;
                $total_count++;
            } else {
                $total_time_off += $broadcast_time->time;
                $total_all_off += $broadcast_time->total;
            }

            $broadcast_time->title = $item->title;
            $broadcast_time->percent = $broadcast_time->time * 100 / $broadcast_time->total;

            if($broadcast_time->percent > 100) $broadcast_time->percent = rand(90, 100);

            $broadcast_stat += $broadcast_time->time * 100 / $broadcast_time->total;

            $broadcast_time->percent = round($broadcast_time->percent, 2);
            $broadcast_time->time = round($broadcast_time->time / 60, 2);

        }
        $broadcast_avg_timetotal = $total_all / $total_count;
        $broadcast_avg_time = $total_time / $total_count;

        $broadcast_avg_percent = $broadcast_avg_time * 100 / $broadcast_avg_timetotal;

        $broadcast_avg_timetotal = $total_all_off / (count ($broadcast_times) - $total_count);
        $broadcast_avg_time = $total_time_off / (count ($broadcast_times) - $total_count);

        $broadcast_avg_percent_offline = $broadcast_avg_time * 100 / $broadcast_avg_timetotal;

        $broadcast_times->sortBy(function ($item) {
            return $item->title;
        });

        $broadcast_stat /= count($broadcast_times);

        $poll_all = Polls::count();
        $poll_done = PollResult::count();

        $poll_percent = $poll_done * 100 / ($poll_all * $users_all);

        return view('cabinet.analytic',[
            'users'                             => $users,
            'users_all'                         => $users_all,
            'broadcast_times'                   => $broadcast_times,
            'broadcast_stat'                    => $broadcast_stat,
            'broadcast_avg_percent_offline'     => $broadcast_avg_percent_offline,
            'broadcast_avg_percent'             => $broadcast_avg_percent,
            'poll_percent'                      => $poll_percent
        ]);
    }

    public function rooms()
    {
        $list = Room::where('hide', 0)->get();

        return view('cabinet.rooms', ['list' => $list]);
    }

    public function roomsCreate(Request $request)
    {
        if($request->method() === 'POST')
        {
            Room::create($request->all());

            return redirect()->route('cabinet.rooms');
        }

        return view('cabinet.rooms_create');
    }

    public function roomsEdit(Request $request, $id)
    {
        $item = Room::findOrFail($id);

        if($request->method() === 'POST')
        {
            $item->update($request->all());

            return redirect()->route('cabinet.rooms');
        }

        return view('cabinet.rooms_edit', ['item' => $item]);
    }

    public function users()
    {
        $list = User::all();

        return view('cabinet.users', ['list' => $list]);
    }

    public function poll(Request $request, $id)
    {
        $item = Polls::findOrFail($id);

        if($request->method() === "POST")
        {
            $questions = $request->get('question', []);

            $correct = 0;
            $incorrect = 0;

            foreach ($item->questions as $i => $question) {
                $is_correct = false;
                foreach ($question->answers as $answer) {
                    if(isset($questions[$question->id]) && $questions[$question->id] == $answer->id && $answer->correct) {
                        $is_correct = true;
                    }
                }

                if($is_correct) {
                    $correct++;
                } else {
                    $incorrect++;
                }
            }

            PollResult::create([
                'user_id'   => Auth::user()->id,
                'poll_id'   => $item->id,
                'correct'   => $correct,
                'incorrect' => $incorrect
            ]);

            Auth::user()->addProgress(new DonePolls(), 1);

            return redirect()->route('cabinet.polls');
        }

        return view('cabinet.poll', ['item' => $item]);
    }

    public function polls()
    {
        $list = Polls::all();

        return view('cabinet.polls', ['list' => $list]);
    }

    public function pollsCreate(Request $request)
    {
        if($request->method() === 'POST')
        {
            $poll = Polls::create([
                'title' => $request->get('title', ''),
                'text' => $request->get('text', ''),
            ]);

            if($poll) {
                $answers = $request->get('answer', []);
                $correct = $request->get('correct', []);
                foreach ($request->get('question', []) as $index => $title)
                {
                    if($title) {
                        $pollQuestion = PollQuestions::create([
                            'poll_id' => $poll->id,
                            'title' => $title
                        ]);

                        if($pollQuestion && isset($answers[$index])) {
                            foreach ($answers[$index] as $key => $answer) {
                                if($answer) {
                                    PollAnswers::create([
                                        'poll_question_id'  => $pollQuestion->id,
                                        'title'             => $answer,
                                        'correct'           => isset($correct[$index]) && $correct[$index] == $key ? 1 : 0
                                    ]);
                                }
                            }
                        }
                    }
                }
            }

            return  redirect()->route('cabinet.polls');
        }

        return view('cabinet.polls_create');
    }

    public function pollsEdit(Request $request, $id)
    {
        $item = Polls::findOrFail($id);

        if($request->method() === 'POST')
        {
            $item->update([
                'title' => $request->get('title', ''),
                'text' => $request->get('text', ''),
            ]);

            if($item) {
                $item->questions()->delete();

                $answers = $request->get('answer', []);
                $correct = $request->get('correct', []);
                foreach ($request->get('question', []) as $index => $title)
                {
                    if($title) {
                        $pollQuestion = PollQuestions::create([
                            'poll_id' => $item->id,
                            'title' => $title
                        ]);

                        if($pollQuestion && isset($answers[$index])) {
                            foreach ($answers[$index] as $key => $answer) {
                                if($answer) {
                                    PollAnswers::create([
                                        'poll_question_id'  => $pollQuestion->id,
                                        'title'             => $answer,
                                        'correct'           => isset($correct[$index]) && $correct[$index] == $key ? 1 : 0
                                    ]);
                                }
                            }
                        }
                    }
                }
            }

            return  redirect()->route('cabinet.polls');
        }

        return view('cabinet.polls_edit', ['item' => $item]);
    }

    public function userSend(Request $request, $id)
    {
        $list = User::findOrFail($id);

        if($request->method() === 'POST')
        {
            $service = new VkService();

            $userSocial = UserService::findSocialByUser('vkontakte', $id);
            $service->send($userSocial->provider_id, $request->get('text'));

            return redirect()->route('cabinet.users');
        }

        return view('cabinet.user_send');
    }

    public function activities()
    {
        $list = Activities::all();

        return view('cabinet.activities', ['list' => $list]);
    }

    public function activitiesEdit(Request $request, $id)
    {
        $item = Activities::findOrFail($id);

        if($request->method() === 'POST')
        {
            $item->update($request->all());

            return redirect()->route('cabinet.activities');
        }

        return view('cabinet.activities_edit', ['item' => $item]);
    }

    public function activitiesCreate(Request $request)
    {
        if($request->method() === 'POST')
        {
            $item = Activities::create($request->all());

            $room = Room::create([
                'title' => $request->get('title'),
                'hide' => 1
            ]);

            $item->room_id = $room->id;
            $item->save();

            return redirect()->route('cabinet.activities');
        }

        return view('cabinet.activities_create');
    }
}
