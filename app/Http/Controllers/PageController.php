<?php

namespace App\Http\Controllers;

use App\Activities;
use App\Room;

class PageController extends Controller
{
    public function __invoke()
    {
        $activities_online = Activities::where('online', 1)->get();
        $activities_offline = Activities::where('online', 0)->get();

        return view('pages.home', [
            'activities_online' => $activities_online,
            'activities_offline' => $activities_offline,
        ]);
    }

    public function about()
    {
        return view('pages.about');
    }

    public function room($id)
    {
        $item = Room::find($id);

        return view('pages.room', ['item' => $item]);
    }
}
