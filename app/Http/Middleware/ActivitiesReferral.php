<?php

namespace App\Http\Middleware;

use Closure;

class ActivitiesReferral
{
    public function handle($request, Closure $next)
    {
        if ($request->hasCookie('activities_referral')) {
            return $next($request);
        }

        $ref = $request->query('a');
        if($ref) {
            list($ref, $activities_id) = explode("|", $ref);
        }

        if ($ref && app(config('referral.user_model', 'App\User'))->referralExists($ref)) {
            return Auth()->check() ? redirect()->route('activities.item', ['id' => $activities_id])->withCookies([
                cookie()->forever('activities_referral', $ref),
                cookie()->forever('activities_referral_id', $activities_id)
            ]) :  redirect()->route('login')->withCookies([
                cookie()->forever('activities_referral', $ref),
                cookie()->forever('activities_referral_id', $activities_id)
            ]);
        }

        return $next($request);
    }
}
