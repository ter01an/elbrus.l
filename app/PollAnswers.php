<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PollAnswers extends Model
{
    protected $fillable = ['poll_question_id', 'title', 'correct'];
}
