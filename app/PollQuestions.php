<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PollQuestions extends Model
{
    protected $fillable = ['poll_id', 'title', 'text'];

    public function answers()
    {
        return $this->hasMany(PollAnswers::class, 'poll_question_id');
    }
}
