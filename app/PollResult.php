<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PollResult extends Model
{
    protected $fillable = ['user_id', 'poll_id', 'correct', 'incorrect'];
}
