<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polls extends Model
{
    protected $fillable = ['title', 'text'];

    public function questions()
    {
        return $this->hasMany(PollQuestions::class, 'poll_id');
    }
}
