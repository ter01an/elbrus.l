<?php

namespace App\Services;

use App\Achievements\DonePolls;
use App\Achievements\ViewActivites;
use App\User;

class AchievementService
{
    public static $statuses = [
        0   => 'Новичок',
        20  => 'Начинающий специалист',
        50  => 'Специалист',
        100 => 'Профи',
    ];

    public static function status(User $user)
    {
        $rating = self::rating($user);

        $status = current(self::$statuses);
        foreach (self::$statuses as $value => $title)
        {
            if($rating >= $value) {
                $status = $title;
            }
        }

        return $status;
    }

    public static function rating(User $user)
    {
        $rating = 0;

        $achievements = self::get();

        foreach ($achievements as $item)
        {
            $first = last($item->class->chain());
            $rating += $user->achievementStatus($first)->points;
        }

        return $rating;
    }

    public static function get()
    {
        return [
            'polls' => (object)[
                'title' => 'Прокачай себя',
                'class' => new DonePolls()
            ],
            'activites' => (object)[
                'title' => 'Участие в мероприятиях',
                'class' => new ViewActivites()
            ]
        ];
    }
}
