<?php

namespace App\Services;

use App\Activities;
use App\User;

class ActivitiesService
{
    public static function getReferralLink(Activities $activities, User $user)
    {
        return url('/') . "?a=" . $user->affiliate_id . "|" . $activities->id;
    }
}
