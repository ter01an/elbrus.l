<?php

namespace App\Services;

use App\PollResult;
use App\User;

class PollService
{
    public static function isEnded(User $user, $poll_id)
    {
        return PollResult::where([
            'user_id' => $user->id,
            'poll_id' => $poll_id
        ])->first();
    }
}
