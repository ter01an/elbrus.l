<?php

namespace App\Services;

use App\Activities;
use App\ActivitiesReferral;
use App\SocialAccount;
use App\User;
use Illuminate\Support\Str;

class UserService
{
    public static function referralUsers(User $user)
    {
        return User::where('referred_by', $user->affiliate_id)->count();
    }

    public static function findOrCreateUser($provider, $socialiteUser)
    {
        if ($user = self::findUserBySocialId($provider, $socialiteUser->getId())) {
            self::ActReferral($user);

            return $user;
        }

        if ($user = self::findUserByEmail($provider, $socialiteUser->getEmail())) {
            self::addSocialAccount($provider, $user, $socialiteUser);

            self::ActReferral($user);

            return $user;
        }

        $ref = request()->cookie('activities_referral', '');

        $user = User::create([
            'name' => $socialiteUser->getName(),
            'email' => $socialiteUser->getEmail(),
            'avatar' => $socialiteUser->getAvatar(),
            'password' => bcrypt(Str::random(25)),
            'referred_by' => $ref
        ]);

        self::ActReferral($user);

        self::addSocialAccount($provider, $user, $socialiteUser);

        return $user;
    }

    public static function ActReferral(User $user)
    {
        $ref = request()->cookie('activities_referral', '');
        $act_ref = request()->cookie('activities_referral_id', 0);
        if($act_ref)
        {
            $referred = User::where('affiliate_id', $ref)->first();
            $act = Activities::find($act_ref);

            if($act && $referred) {
                ActivitiesReferral::create([
                    'user_id' => $user->id,
                    'referred_id' => $referred->id,
                    'activities_id' => $act_ref
                ]);
            }
        }
    }

    public static function addSocialAccount($provider, $user, $socialiteUser)
    {
        SocialAccount::create([
            'user_id' => $user->id,
            'provider' => $provider,
            'provider_id' => $socialiteUser->getId(),
            'token' => $socialiteUser->token,
        ]);
    }

    public static function findUserByEmail($provider, $email)
    {
        return !$email ? null : User::where('email', $email)->first();
    }

    public static function findUserBySocialId($provider, $id)
    {
        $socialAccount = SocialAccount::where('provider', $provider)->where('provider_id', $id)->first();

        return $socialAccount ? $socialAccount->user : false;
    }

    public static function findSocialByUser($provider, $id)
    {
        return SocialAccount::where('user_id', $id)->where('provider', $provider)->first();
    }
}
