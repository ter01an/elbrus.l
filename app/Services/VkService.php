<?php

namespace App\Services;


class VkService {

    protected $access_token = '1c90299daf125f41be4cb2b8d3299daccccc84d58970eaabb8c31c378ebb289a70ccf5fd9d437a9bb38c0';

    public function send($id, $message)
    {
        $url = 'https://api.vk.com/method/messages.send';

        $params = array(
            'user_id' => $id,
            'message' => $message,
            'access_token' => $this->access_token,
            'v' => '5.37',
        );

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));
    }

}
