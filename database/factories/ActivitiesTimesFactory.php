<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\ActivitiesTime;

$factory->define(ActivitiesTime::class, function (Faker $faker) {
    $total = rand(10 * 60, 120 * 60);
    $current = abs($total - rand(10 * 60, 120 * 60));
    return [
        'user_id' => function() {
            return \App\User::inRandomOrder()->first()->id;
        },
        'activities_id' => function() {
            return \App\Activities::inRandomOrder()->first()->id;
        },
        'time' => $current,
        'total' => 120 * 60
    ];
});
