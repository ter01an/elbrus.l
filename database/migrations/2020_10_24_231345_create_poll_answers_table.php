<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePollAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poll_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('poll_question_id')->unsigned();
            $table->string('title');
            $table->tinyInteger('correct');

            $table->timestamps();

            $table->foreign('poll_question_id')
                ->references('id')
                ->on('poll_questions')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poll_answers');
    }
}
