import IMask from 'imask';

let element = document.getElementById('register-input');
if(element) {
    let maskOptions = {
        mask: '+{7} (000) 000-00-00',
        lazy: false
    };
    let mask = IMask(element, maskOptions);
}
