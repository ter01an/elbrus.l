import React from 'react';
import ReactDOM from 'react-dom';
import { ChatStateProvider } from "./components/ChatState";
import ChatRoom from "./components/ChatRoom";
import { SWRConfig } from 'swr';
import Echo from "laravel-echo"

window.io = require('socket.io-client');
window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});

let chatRoom = document.getElementById('chat-room');
if (chatRoom) {
    ReactDOM.render(
        <SWRConfig value={{ fetcher: (resource, init) => fetch(resource, init).then(res => res.json()) }}>
            <ChatStateProvider room={chatRoom.dataset.room} user={chatRoom.dataset.user}>
                <ChatRoom />
            </ChatStateProvider>
        </SWRConfig>
        , document.getElementById('chat-room')
    );
}
