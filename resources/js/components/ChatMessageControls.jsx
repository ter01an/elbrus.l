import React, { useState, useRef } from 'react';
import { useChatState } from "./ChatState";
import useRoomMessages from "../stores/useRoomMessages";

export default function ChatMessageControls (props) {
    const refForm = useRef();
    const { room } = useChatState();
    const [ message, setMessage ] = useState('');
    const { mutateMessages } = useRoomMessages(room);

    async function sendMessage(e) {
        e.preventDefault();

        let formData = new FormData();
        formData.append('to_id', room);
        formData.append('to_type', 'room');
        formData.append('text', message);

        setMessage('');
        refForm.current.reset();

        const response = await fetch('/api/v1/message', {
            headers: {
                'Accept': 'application/json'
            },
            method: "POST",
            body: formData,
            credentials: 'include'
        });

        const item = await response.json();

        await mutateMessages(async list => {
            list.push(item);
            return list
        });
    }

    async function onEnterPress(e) {
        if(e.keyCode == 13 && e.shiftKey == false) {
            e.preventDefault();
            await sendMessage(e);
        }
    }

    return (
        <form ref={refForm} onSubmit={sendMessage} className={"chat-controls"}>
            <div className={"d-flex align-items-start"}>
                <div className="pr-3 flex-grow-1 flex-shrink-1">
                    <textarea required={true} placeholder={"Введите сообщение"} className={"form-control"} name="message" onKeyDown={onEnterPress} onInput={e => setMessage(e.target.value)}/>
                </div>
                <div className={"flex-grow-0 flex-shrink-0"}>
                    <button type={"submit"} className={"btn btn-success"}><i className="fas fa-paper-plane"/></button>
                </div>
            </div>
        </form>
    );
}
