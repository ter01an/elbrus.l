import React, { useEffect, useState, useRef } from 'react';
import { useChatState } from "./ChatState";
import useRoomUsers from "../stores/useRoomUsers";
import useRoomMessages from "../stores/useRoomMessages";
import { keyBy, find } from "lodash";
import ChatMessage from "./ChatMessage";

export default function ChatMessagesArea() {
    const refChat = useRef(null);
    const { room, user } = useChatState();
    const [ usersByID, setUsersByID ] = useState({});
    const { users, mutateUsers } = useRoomUsers(room);
    const { messages, mutateMessages } = useRoomMessages(room);

    useEffect(() => {
        Echo.channel(`room.${room}`)
        .listen('.Message.New', async (e) => {
            mutateMessages(async list => {
                if(!find(list, item => item.id == e.id)) {
                    list.push(e);
                }

                return list
            })
        });
    }, []);

    useEffect(() => {
        if(refChat.current) {
            refChat.current.scrollTop = refChat.current.scrollHeight;
        }
    }, [messages ? messages.length : -1, typeof users === "undefined" || typeof messages === "undefined"]);

    useEffect(() => {
        if(users) {
            setUsersByID(keyBy(users, 'id'));
        }
    }, [users ? users.length : -1])

    if(typeof users === "undefined" || typeof messages === "undefined") {
        return (
            <div className={"chat-discussion loading"}/>
        )
    }

    return (
        <div ref={refChat} className={"chat-discussion"}>
            {messages.map(message => ( message.from_id in usersByID &&
                <ChatMessage key={message.id} message={message} users={usersByID}/>
            ))}
        </div>
    )
}
