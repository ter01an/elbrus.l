import React from 'react';
import ChatMessageControls from "./ChatMessageControls";
import ChatMessagesArea from "./ChatMessagesArea";
import ChatUserList from "./ChatUserList";
import {useChatState} from "./ChatState";

export default function ChatRoom() {
    const { room, user } = useChatState();

    return (
        <div className={"ibox chat-view"}>
            <div className={"ibox-content"}>
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <ChatUserList/>
                    </div>
                    <div className={"col-md-9"}>
                        <ChatMessagesArea/>
                    </div>
                </div>
            </div>
            {user &&
                <div className={"pt-2"}>
                    <ChatMessageControls/>
                </div>
            }
        </div>
    )
}
