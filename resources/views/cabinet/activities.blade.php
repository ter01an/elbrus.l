@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            <div class="row">
                <div class="col-12 col-md-8 order-2 order-md-1">Мероприятия</div>
                @role('admin')
                    <div class="col-12 col-md-4 order-1 order-md-2 mb-3 mb-md-0"><a href="{{ route('cabinet.activities.create') }}" class="btn">Создать</a></div>
                @endrole
            </div>
        </div>

        <div class="box__body">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Тип</th>
                        <th>Реферальная ссылка</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->online ? 'Онлайн' : 'Офлайн' }}</td>
                        @role('admin')
                            <td>
                                <span class="mr-3">{{ \App\Services\ActivitiesService::getReferralLink($item, Auth()->user()) }}</span>
                                <a target="_blank" href="{{ route('activities.qr', ['id' => $item->id]) }}" class="btn"><i class="fas fa-qrcode"></i></a>
                            </td>
                        @endrole
                        <td>
                            <a href="{{ route('activities.item', ['id' => $item->id]) }}" target="_blank">Перейти на страницу</a>
                        </td>
                        @role('admin')
                            <td width="1%">
                                <a href="{{ route('cabinet.activities.edit', ['id' => $item->id]) }}" class="btn">Редактировать</a>
                            </td>
                        @endrole
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
