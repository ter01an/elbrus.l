@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            <div class="row">
                <div class="col-12 col-md-8 order-2 order-md-1">Редактировать мероприятие</div>
                <div class="col-12 col-md-4 order-1 order-md-2 mb-3 mb-md-0"><a href="{{ route('cabinet.activities') }}" class="btn">Назад</a></div>
            </div>
        </div>
        <div class="box__body">
            <form method="post">
                @csrf

                <div class="fields">
                    <label>
                        <input type="hidden" name="online" value="0">
                        <input type="checkbox" name="online" value="1" {{ $item->online ? 'checked' : '' }} />
                        Онлайн мероприятие
                    </label>
                    <input type="text" class="input" name="title" placeholder="Название" value="{{ $item->title }}" />
                    <textarea class="textarea" name="text" placeholder="Описание">{{ $item->text }}</textarea>
                    <textarea class="textarea" name="embed" placeholder="Встраиваемый контент">{{ $item->embed }}</textarea>
                    <button type="submit" class="btn">Сохранить</button>
                </div>
            </form>
        </div>
    </div>

@endsection
