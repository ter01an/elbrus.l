@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            Аналитика
        </div>
        <div class="box__body">
            <div class="mb-15">
                <h4>Общее колво пользователей: {{ $users_all }}</h4>
                <canvas data-items="{{ json_encode($users) }}" id="charts-users" style="width: 100%; height: 400px;"></canvas>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="mb-15">
                        <h4>Вовлеченность в онлайн мероприятия % полноты просмотра: {{ round($broadcast_avg_percent, 2) }}%</h4>
                        <canvas data-percent="{{ round($broadcast_avg_percent, 0) }}" id="charts-broadcast-avg" style="width: 100%; height: 400px;"></canvas>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-15">
                        <h4>Вовлеченность в оффлайн мероприятия % полноты просмотра: {{ round($broadcast_avg_percent_offline, 2) }}%</h4>
                        <canvas data-percent="{{ round($broadcast_avg_percent_offline, 0) }}" id="charts-broadcast-avg-off" style="width: 100%; height: 400px;"></canvas>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-15">
                        <h4>Вовлеченность в прохождения тестов: {{ round($poll_percent, 2) }}%</h4>
                        <canvas data-percent="{{ round($poll_percent, 0) }}" id="charts-poll" style="width: 100%; height: 400px;"></canvas>
                    </div>
                </div>
            </div>

            <div class="mb-15">
                <h4>Вовлеченность в разрезе интереса к мероприятию. Средняя продолжительность просмотра трансляций: {{ round($broadcast_stat, 2) }}%</h4>
                <canvas data-items="{{ json_encode($broadcast_times) }}" id="charts-broadcast" style="width: 100%; height: 400px;"></canvas>
            </div>
        </div>
    </div>

@endsection
