
<h4>Вопрос #{{ $number + 1 }}</h4>

<div class="fields">
    <input type="text" name="question[{{ $number }}]" class="input" placeholder="Вопрос" value="{{ isset($questions[$number]) ? $questions[$number]['title'] : '' }}">
    <div>
        <div class="d-flex flex-wrap flex-md-nowrap">
            <input type="text" name="answer[{{ $number }}][1]" class="input" placeholder="Ответ" value="{{ isset($answers[$number][0]) ? $answers[$number][0]['title'] : '' }}">
            <label class="mt-2 mt-md-0">
                <input type="radio" required name="correct[{{ $number }}]" value="1" checked {{ isset($answers[$number][0]) && $answers[$number][0]['correct'] ? 'checked' : '' }}>
                Правильный ответ
            </label>
        </div>
    </div>
    <div>
        <div class="d-flex flex-wrap flex-md-nowrap">
            <input type="text" name="answer[{{ $number }}][2]" class="input" placeholder="Ответ" value="{{ isset($answers[$number][1]) ? $answers[$number][1]['title'] : '' }}">
            <label class="mt-2 mt-md-0">
                <input type="radio" required name="correct[{{ $number }}]" value="2" {{ isset($answers[$number][1]) && $answers[$number][1]['correct'] ? 'checked' : '' }}>
                Правильный ответ
            </label>
        </div>
    </div>
    <div>
        <div class="d-flex flex-wrap flex-md-nowrap">
            <input type="text" name="answer[{{ $number }}][3]" class="input" placeholder="Ответ" value="{{ isset($answers[$number][2]) ? $answers[$number][2]['title'] : '' }}">
            <label class="mt-2 mt-md-0">
                <input type="radio" required name="correct[{{ $number }}]" value="3" {{ isset($answers[$number][2]) && $answers[$number][2]['correct'] ? 'checked' : '' }}>
                Правильный ответ
            </label>
        </div>
    </div>
    <div>
        <div class="d-flex flex-wrap flex-md-nowrap">
            <input type="text" name="answer[{{ $number }}][4]" class="input" placeholder="Ответ" value="{{ isset($answers[$number][3]) ? $answers[$number][3]['title'] : '' }}">
            <label class="mt-2 mt-md-0">
                <input type="radio" required name="correct[{{ $number }}]" value="4" {{ isset($answers[$number][3]) && $answers[$number][3]['correct'] ? 'checked' : '' }}>
                Правильный ответ
            </label>
        </div>
    </div>
</div>
