@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            Личный кабинет
        </div>
        <div class="box__body">
            <div class="user flex-wrap">
                <div class="d-flex flex-wrap">
                    <div class="user__avatar">
                        <img src="{{ $user->avatar ? $user->avatar : 'https://picsum.photos/300/200?random=0' }}" alt="avatar">
                    </div>
                    <div class="user__info">
                        <div class="user__name">
                            {{ $user->name }}
                        </div>
                        <div class="user__email">
                            {{ $user->email }}
                        </div>
                        <div class="user__phone">
                            {{ $user->phone }}
                        </div>
                        @if(Auth()->check() && $user->id == Auth()->user()->id)
                            <div id="vk_send_message" class="mt-2"></div>
                            <script type="text/javascript">
                                VK.Widgets.AllowMessagesFromCommunity("vk_send_message", {height: 30}, 199717637);
                            </script>
                        @endif
                    </div>
                </div>
                <div class="user__progress flex-wrap">
                    <div class="user-status">
                        <div class="user-status__title">Рейтинг</div>
                        <div class="user-status__detail">
                            <i class="fas fa-trophy"></i>
                            {{ \App\Services\AchievementService::rating($user) * 15 }}
                        </div>
                    </div>
                    <div class="user-status">
                        <div class="user-status__title">Звание</div>
                        <div class="user-status__detail">
                            <i class="far fa-hand-peace"></i>
                            {{ \App\Services\AchievementService::status($user) }}
                        </div>
                    </div>
                    @if(Auth()->check() && $user->id == Auth()->user()->id)
                        <div class="user-status">
                            <div class="user-status__title">Реферальная ссылка</div>
                            <div class="user-status__detail">
                                <i class="fas fa-share"></i>
                                {{ $user->getReferralLink() }}
                            </div>
                            <div class="user-status__title">Приглашено: {{ \App\Services\UserService::referralUsers($user) }}</div>
                        </div>
                        <div class="user-status">
                            <div class="user-status__title">Реферальная ссылка</div>
                            <div class="user-status__detail">
                                {!! QrCode::size(100)->generate($user->getReferralLink()); !!}
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <h3>Достижения</h3>

            <div class="achievements">
                @foreach($achievements as $achievement)
                    <div class="achievement box mb-15">
                        <div class="achievement__title box__title">
                            {{ $achievement->title }}
                        </div>
                        <div class="achievement-list">
                            @foreach($achievement->class->chain() as $item)
                                <div class="box achievement-item {{ $item->id }}">
                                    <div class="box__title achievement-item__title">{{ $item->name }}</div>
                                    <div class="box__body">
                                        @if(!$user->achievementStatus($item)->unlocked_at)
                                            <div class="achievement-item__progress">
                                                <div style="width: {{ $user->achievementStatus($item)->points * 100 / $item->getPoints() }}%"></div>
                                                <span>{{ $user->achievementStatus($item)->points }} / {{ $item->getPoints() }}</span>
                                            </div>
                                        @else
                                            <div class="achievement-item__unlock">
                                                {{ $item->description }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
