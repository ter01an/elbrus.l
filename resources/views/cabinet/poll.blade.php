@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            <div class="row">
                <div class="col-12 col-md-8 order-2 order-md-1">{{ $item->title }}</div>
                <div class="col-12 col-md-4 order-1 order-md-2 mb-3 mb-md-0"><a href="{{ route('cabinet.polls') }}" class="btn">Назад</a></div>
            </div>
        </div>
        <div class="box__body">
            <form method="post">
                @csrf
                @foreach($item->questions as $question)
                    <div class="box mb-15">
                        <div class="box__title box__title--mini">
                            {{ $question->title }}
                        </div>
                        <div class="box__body">
                            <div class="fields">
                                @foreach($question->answers as $answer)
                                    <label>
                                        <input type="radio" required name="question[{{ $question->id }}]" value="{{ $answer->id }}">
                                        {{ $answer->title }}
                                    </label>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach

                <button type="submit" class="btn">Отправить</button>
            </form>
        </div>
    </div>

@endsection
