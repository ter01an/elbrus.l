@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            Тесты

            @role('admin')
                <a href="{{ route('cabinet.polls.create') }}" class="btn">Создать</a>
            @endrole
        </div>

        <div class="box__body">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    @role('admin')
                        <th></th>
                    @endrole
                </tr>
                </thead>
                <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->title }}</td>
                        <td width="1%" style="white-space: nowrap">
                            @php
                              $result = \App\Services\PollService::isEnded(Auth()->user(), $item->id);
                            @endphp
                            @if($result)
                                Пройден: {{ $result->correct }} / {{ $result->correct + $result->incorrect }}
                            @else
                                <a href="{{ route('cabinet.poll', ['id' => $item->id]) }}" class="btn">Пройти тест</a>
                            @endif
                        </td>
                        @role('admin')
                            <td width="1%">
                                <a href="{{ route('cabinet.polls.edit', ['id' => $item->id]) }}" class="btn">Редактировать</a>
                            </td>
                        @endrole
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
