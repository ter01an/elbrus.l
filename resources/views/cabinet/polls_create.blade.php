@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            <div class="row">
                <div class="col-12 col-md-8 order-2 order-md-1">Создать тест</div>
                <div class="col-12 col-md-4 order-1 order-md-2 mb-3 mb-md-0"><a href="{{ route('cabinet.polls') }}" class="btn">Назад</a></div>
            </div>
        </div>
        <div class="box__body">
            <form method="post">
                @csrf

                <div class="fields">
                    <input type="text" required name="title" class="input" placeholder="Название">
                    <textarea class="textarea" name="text" placeholder="Описание"></textarea>
                </div>

                @for ($i = 1; $i < 5; $i++)
                    @include('cabinet.inc.question', ['number' => $i])
                @endfor

                <button type="submit" class="btn">Создать</button>
            </form>
        </div>
    </div>

    <script>
        (function ($){
            $(function (){

            })
        }(jQuery))
    </script>

@endsection
