@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            <div class="row">
                <div class="col-12 col-md-8 order-2 order-md-1">Редактирование теста</div>
                <div class="col-12 col-md-4 order-1 order-md-2 mb-3 mb-md-0"><a href="{{ route('cabinet.polls') }}" class="btn">Назад</a></div>
            </div>
        </div>
        <div class="box__body">
            <form method="post">
                @csrf

                <div class="fields">
                    <input type="text" required name="title" class="input" placeholder="Название" value="{{ $item->title }}">
                    <textarea class="textarea" name="text" placeholder="Описание">{{ $item->text }}</textarea>
                </div>

                @php
                    if(isset($item)) {
                        $questions = $item->questions->toArray();
                        $answers = [];

                        foreach ($item->questions as $i => $question) {
                            foreach ($question->answers as $answer) {
                                $answers[$i][] = $answer->toArray();
                            }
                        }
                    }
                @endphp

                @for ($i = 0; $i < 5; $i++)
                    @include('cabinet.inc.question', ['number' => $i, 'questions' => $questions, 'answers' => $answers])
                @endfor

                <button type="submit" class="btn">Сохранить</button>
            </form>
        </div>
    </div>

@endsection
