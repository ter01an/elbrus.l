@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            Беседы

            @role('admin')
                <a href="{{ route('cabinet.rooms.create') }}" class="btn">Создать</a>
            @endrole
        </div>

        <div class="box__body">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th></th>
                    @role('admin')
                        <th></th>
                    @endrole
                </tr>
                </thead>
                <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->title }}</td>
                        <td>
                            <a href="{{ route('room', ['id' => $item->id]) }}">Перейти</a>
                        </td>
                        @role('admin')
                            <td width="1%">
                                <a href="{{ route('cabinet.rooms.edit', ['id' => $item->id]) }}" class="btn">Редактировать</a>
                            </td>
                        @endrole
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
