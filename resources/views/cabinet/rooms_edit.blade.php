@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            <div class="row">
                <div class="col-12 col-md-8 order-2 order-md-1">Редактировать беседу</div>
                <div class="col-12 col-md-4 order-1 order-md-2 mb-3 mb-md-0"><a href="{{ route('cabinet.rooms') }}" class="btn">Назад</a></div>
            </div>
        </div>
        <div class="box__body">
            <form method="post">
                @csrf

                <div class="fields">
                    <input type="text" class="input" name="title" placeholder="Название" value="{{ $item->title }}" />
                    <button type="submit" class="btn">Сохранить</button>
                </div>
            </form>
        </div>
    </div>

@endsection
