@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            <div class="row">
                <div class="col-12 col-md-8 order-2 order-md-1">Рассылка в соц сети</div>
                <div class="col-12 col-md-4 order-1 order-md-2 mb-3 mb-md-0"><a href="{{ route('cabinet.users') }}" class="btn">Назад</a></div>
            </div>
        </div>

        <div class="box__body">
            <form method="post">
                @csrf
                <div class="fields">
                    <textarea class="textarea" name="text" placeholder="Текст"></textarea>
                    <button type="submit" class="btn">Отправить</button>
                </div>
            </form>
        </div>
    </div>

@endsection
