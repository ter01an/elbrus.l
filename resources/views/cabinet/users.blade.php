@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            Пользователи
        </div>

        <div class="box__body">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Имя</th>
                    <th>Телефон</th>
                    <th>Емаил</th>
                    <th>Привлечено</th>
                    <th>Соц. сети</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>
                            <a href="{{ route('cabinet.user', ['id' => $item->id]) }}">{{ $item->name }}</a>
                        </td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ \App\Services\UserService::referralUsers($item) }}</td>
                        <td>
                            @foreach($item->socials as $item)
                                @if($item->provider == 'vkontakte')
                                    <i class="fab fa-vk"></i>
                                @endif
                            @endforeach
                        </td>
                        <td width="1%">
                            <a href="{{ route('cabinet.user.send', ['id' => $item->id]) }}" class="btn">Рассылка в соц. сети</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
