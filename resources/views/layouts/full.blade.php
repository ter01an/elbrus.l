<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ mix('css/chats.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons.css') }}">

    <script src="https://vk.com/js/api/openapi.js?168" type="text/javascript"></script>
</head>
<body>

<div id="app">
    @auth
        <div class="sidebar">
            <div class="sidebar__logo">
                <a href="{{ route('home') }}">
                    <img src="/images/logo-white.svg" alt="Эльбрус">
                </a>
            </div>
            <div class="sidebar__menu">
                <ul>
                    <li>
                        <a href="{{ route('cabinet.activities') }}">
                            <i class="fas fa-graduation-cap"></i>
                            <span>Мероприятия</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('cabinet.rooms') }}">
                            <i class="fab fa-chromecast"></i>
                            <span>Беседы</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('cabinet.polls') }}">
                            <i class="fas fa-poll"></i>
                            <span>Тесты</span>
                        </a>
                    </li>
                    @role('admin')
                    <li>
                        <a href="{{ route('cabinet.users') }}">
                            <i class="fas fa-users"></i>
                            <span>Пользователи</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('cabinet.analytic') }}">
                            <i class="fas fa-chart-area"></i>
                            <span>Аналитика</span>
                        </a>
                    </li>
                    @endrole
                </ul>
            </div>
        </div>
    @endauth
    <div class="content">
        <div class="panel">
            <div class="panel__row">
                @guest
                    <div class="panel__logo">
                        <a href="{{ route('home') }}">
                            <img src="/images/logo.svg" alt="Эльбрус">
                        </a>
                    </div>
                @endguest
                <div class="panel__menu d-none d-lg-block">
                    <ul>
                        <li>
                            <a href="#">О клубе</a>
                        </li>
                        <li>
                            <a href="#">Проекты</a>
                        </li>
                        <li>
                            <a href="#">Новости</a>
                        </li>
                    </ul>
                </div>
                <div class="panel__auth ml-auto">
                    @auth
                        <a href="{{ route('cabinet') }}" class="panel__user">
                            @if(Auth::user()->avatar)
                                <img src="{{ Auth::user()->avatar }}" alt="avatar">
                            @endif
                            <span class="d-none d-md-inline-block">{{ Auth::user()->name }}</span>
                        </a>
                        <a href="{{ route('logout') }}">
                            <i class="fas fa-sign-out-alt"></i> Выйти
                        </a>
                    @endauth
                    @guest
                        <a href="{{ route('login') }}"><i class="fa fa-sign-in-alt"></i> Войти</a>
                    @endguest
                </div>
            </div>
        </div>

        @yield('content')
    </div>
</div>

<!-- Scripts -->
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ mix('js/chats.js') }}"></script>
</body>
</html>
