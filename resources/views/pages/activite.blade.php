@extends('layouts.full')

@section('content')

    <div class="box__title">
        {{ $item->title }}

        <div>
            @if(Auth()->check())
                @if($item->users->contains(Auth()->user()->id))
                    <a href="{{ route('activities.item.unreg', ['id' => $item->id]) }}" class="btn">Не смогу участвовать</a>
                @else
                    <a href="{{ route('activities.item.reg', ['id' => $item->id]) }}" class="btn">Рег. на мероприятие</a>
                @endif
            @else
                <a href="{{ route('login') }}" class="btn">Рег. на мероприятие</a>
            @endif
        </div>
    </div>

    {!! $item->embed !!}

    <div class="box box-fixed">
        <div class="box__body">
            @if($item->online)
                <div id="chat-room" data-room="{{ $item->room_id }}" data-user="{{ \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : "" }}"></div>
            @endif
        </div>
    </div>

    @if($item->text)
        <div class="box">
            <div class="box__body">
                {!! $item->text !!}
            </div>
        </div>
    @endif

@endsection
