@extends('layouts.app')

@section('content')

    <div class="box mb-15">
        <div class="box__title">Онлайн-курсы</div>
        <div class="box__body">
            <div class="act-list justify-content-center justify-content-md-start">
                @foreach($activities_online as $i => $item)
                    <a href="{{ route('activities.item', ['id' => $item->id]) }}">
                        <div class="act">
                            <div class="act__image">
                                <img src="https://picsum.photos/300/200?random={{ $i }}" alt="">
                            </div>
                            <div class="act__title">
                                {{ $item->title }}
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box__title">Офлайн-курсы</div>
        <div class="box__body">
            <div class="act-list justify-content-center justify-content-md-start">
                @foreach($activities_offline as $i => $item)
                    <a href="{{ route('activities.item', ['id' => $item->id]) }}">
                        <div class="act">
                            <div class="act__image">
                                <img src="https://picsum.photos/300/200?random={{ $i + 10 }}" alt="">
                            </div>
                            <div class="act__title">
                                {{ $item->title }}
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
