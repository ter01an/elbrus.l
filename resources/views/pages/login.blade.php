@extends('layouts.app')

@section('content')

    <div class="auth">
        <h3>Войти</h3>
        <ul>
            <li>
                <a href="{{ route('auth.social', 'vkontakte') }}" class="auth__vk">
                    <i class="fab fa-vk"></i>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);" class="auth__inst" style="filter: grayscale(100);">
                    <i class="fab fa-instagram"></i>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);" class="auth__telegram" style="filter: grayscale(100);">
                    <i class="fab fa-telegram-plane"></i>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);" class="auth__whatsapp" style="filter: grayscale(100);">
                    <i class="fab fa-whatsapp"></i>
                </a>
            </li>
        </ul>
    </div>

@endsection
