@extends('layouts.app')

@section('content')

    <div class="auth">
        <h3>Регистрация</h3>
        <form method="post" class="register">
            @csrf
            <div class="row">
                <div class="col-12 col-md-8">
                    <input required name="phone" id="register-input" class="input" type="text" placeholder="Номер телефона">
                </div>
                <div class="col-12 col-md-4">
                    <button type="submit" class="btn w-100 mt-2 mt-md-0">Зарегистрироваться</button>
                </div>
            </div>
        </form>
    </div>

@endsection
