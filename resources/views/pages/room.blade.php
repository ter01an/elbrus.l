@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="box__title">
            {{ $item->title }}
        </div>
        <div class="box__body">
            <div id="chat-room" data-room="{{ $item->id }}" data-user="{{ \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : "" }}"></div>
        </div>
    </div>

@endsection
