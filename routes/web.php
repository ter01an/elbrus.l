<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/pre', function (){
    //$role = \Spatie\Permission\Models\Role::create(['name' => 'admin']);

    $user = \App\User::find(1);
    $act = \App\Activities::find(1);

    /*echo '<pre>';
    var_dump($act->users);
    echo '</pre>';*/
    //$user->assignRole('admin');

    //$user->addProgress(new \App\Achievements\ViewActivites(), 1);
});

Route::group(['middleware' => ['referral', 'activities_referral']], function() {
    Route::get('/', 'PageController')->name('home');

    Route::get('/login', 'Auth\SocialController@login')->name('login');
    Route::get('/logout', 'Auth\SocialController@logout')->name('logout');
    Route::any('/register', 'Auth\SocialController@register')->name('register');

    Route::group(['prefix' => 'social-auth'], function (){
        Route::get('{provider}', 'Auth\SocialController@redirectToProvider')->name('auth.social');
        Route::get('{provider}/callback', 'Auth\SocialController@handleProviderCallback')->name('auth.social.callback');
    });

    Route::get('/about', 'PageController@about')->name('about');

    Route::get('/activities/{id}', 'ActivitiesController@item')->name('activities.item');
    Route::get('/activities/{id}/reg', 'ActivitiesController@itemReg')->name('activities.item.reg');
    Route::get('/activities/{id}/unreg', 'ActivitiesController@itemUnreg')->name('activities.item.unreg');
    Route::get('/activities/qr/{id}', 'ActivitiesController@qr')->name('activities.qr');
    Route::get('/rooms/{id}', 'PageController@room')->name('room');

    Route::group(['prefix' => 'cabinet', 'middleware' => 'auth'], function(){
        Route::get('/', 'CabinetController')->name('cabinet');
        Route::get('/polls', 'CabinetController@polls')->name('cabinet.polls');
        Route::get('/activities', 'CabinetController@activities')->name('cabinet.activities');
        Route::get('/rooms', 'CabinetController@rooms')->name('cabinet.rooms');

        Route::group(['middleware' => 'role:admin'], function (){
            Route::any('/activities/create', 'CabinetController@activitiesCreate')->name('cabinet.activities.create');
            Route::any('/activities/edit/{id}', 'CabinetController@activitiesEdit')->name('cabinet.activities.edit');

            Route::any('/polls/create', 'CabinetController@pollsCreate')->name('cabinet.polls.create');
            Route::any('/polls/edit/{id}', 'CabinetController@pollsEdit')->name('cabinet.polls.edit');

            Route::get('/users', 'CabinetController@users')->name('cabinet.users');
            Route::any('/users/{id}/send', 'CabinetController@userSend')->name('cabinet.user.send');

            Route::any('/rooms/create', 'CabinetController@roomsCreate')->name('cabinet.rooms.create');
            Route::any('/rooms/edit/{id}', 'CabinetController@roomsEdit')->name('cabinet.rooms.edit');

            Route::get('/analytic', 'CabinetController@analytic')->name('cabinet.analytic');
        });

        Route::get('/user/{id}', 'CabinetController')->name('cabinet.user');
        Route::any('/polls/{id}', 'CabinetController@poll')->name('cabinet.poll');
    });
});

